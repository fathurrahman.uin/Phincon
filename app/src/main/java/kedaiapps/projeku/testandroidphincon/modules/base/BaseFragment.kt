package kedaiapps.projeku.testandroidphincon.modules.base

import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract  class BaseFragment() : Fragment()