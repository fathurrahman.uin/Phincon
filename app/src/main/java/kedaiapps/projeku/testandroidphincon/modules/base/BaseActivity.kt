package kedaiapps.projeku.testandroidphincon.modules.base

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract class BaseActivity() : AppCompatActivity()