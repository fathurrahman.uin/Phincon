package kedaiapps.projeku.testandroidphincon.services.rest

import kedaiapps.projeku.testandroidphincon.services.Response
import kedaiapps.projeku.testandroidphincon.services.entity.*
import retrofit2.http.*

interface MainRest {

    //home
    @GET("pokemon")
    suspend fun home() : Response<List<ResponseHome>>

    //search
    @GET("pokemon/{name}")
    suspend fun homeDetail(
        @Path("name") id: String,
    ) : ResponseHomeDetail
}