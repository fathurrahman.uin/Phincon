package kedaiapps.projeku.testandroidphincon.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kedaiapps.projeku.testandroidphincon.db.table.FavoriteTable
import kedaiapps.projeku.testandroidphincon.db.table.ListPokemonTable

@Database(entities = [ListPokemonTable::class, FavoriteTable::class], version = 1)
abstract class PhinconDB : RoomDatabase(){

    companion object {
        private var INSTANCE: PhinconDB? = null

        fun getDatabase(context: Context): PhinconDB {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, PhinconDB::class.java, "PhinconDB")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return INSTANCE as PhinconDB
        }
    }

    abstract fun daoListPokemon() : daoListPokemon
    abstract fun daoFavorite() : daoFavorite
}