package kedaiapps.projeku.testandroidphincon.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kedaiapps.projeku.testandroidphincon.db.table.FavoriteTable

@Dao
interface daoFavorite {
    @Query("SELECT * FROM FavoriteTable")
    fun getAll() : LiveData<List<FavoriteTable>>

    @Query("SELECT * FROM FavoriteTable WHERE fav_id=:fav_id")
    fun getById(fav_id: String): LiveData<FavoriteTable>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg data: FavoriteTable)

    @Query("UPDATE FavoriteTable SET name=:name, status=:status WHERE fav_id=:fav_id")
    fun update(fav_id: String, name: String, status: String)

    @Query("DELETE FROM FavoriteTable")
    fun deleteAll()
}