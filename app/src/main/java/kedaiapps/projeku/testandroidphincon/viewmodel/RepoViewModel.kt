package kedaiapps.projeku.testandroidphincon.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kedaiapps.projeku.testandroidphincon.db.PhinconDB
import kedaiapps.projeku.testandroidphincon.db.table.FavoriteTable
import kedaiapps.projeku.testandroidphincon.db.table.ListPokemonTable
import kedaiapps.projeku.testandroidphincon.ext.ioThread
import javax.inject.Inject

@HiltViewModel
class RepoViewModel @Inject constructor(
    val application: Application
) : ViewModel() {

    val db = PhinconDB.getDatabase(this.application)

    //======================= Local Database ListPokemon ===================
    fun setListPokemon(name: String, url: String) {
        ioThread {
            db.daoListPokemon().insert(ListPokemonTable(0, name, url))
        }
    }

    fun getListPokemon(): LiveData<List<ListPokemonTable>> {
        return db.daoListPokemon().getAll()
    }

    fun getListPokemonId(fav_id: String): LiveData<ListPokemonTable> {
        return db.daoListPokemon().getById(fav_id)
    }

//    fun updateListPokemon(fav_id: String, status: String){
//        ioThread {
//            db.daoListPokemon().update(fav_id, status)
//        }
//    }

    fun deleteListPokemon() {
        ioThread {
            db.daoListPokemon().deleteAll()
        }
    }


    //======================= Local Database Favorite ===================
    fun setFavorite(fav_id: String, background_image: String, name: String, weight: String, status: String){
        ioThread {
            db.daoFavorite().insert(FavoriteTable(0, fav_id, background_image, name, weight, status))
        }
    }

    fun getFavorite(): LiveData<List<FavoriteTable>> {
        return db.daoFavorite().getAll()
    }

    fun getFavoriteId(fav_id: String) : LiveData<FavoriteTable> {
        return db.daoFavorite().getById(fav_id)
    }

    fun updateFavorite(fav_id: String, name: String, status: String){
        ioThread {
            db.daoFavorite().update(fav_id, name, status)
        }
    }

    fun deleteFavorite(){
        ioThread {
            db.daoFavorite().deleteAll()
        }
    }
}