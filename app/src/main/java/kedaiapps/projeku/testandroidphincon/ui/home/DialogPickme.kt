package kedaiapps.projeku.testandroidphincon.ui.home

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.core.view.isVisible
import kedaiapps.projeku.testandroidphincon.R
import kedaiapps.projeku.testandroidphincon.databinding.DialogPickmeBinding
import kedaiapps.projeku.testandroidphincon.ext.toast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DialogPickme (val context: Activity): Dialog(context) {

    lateinit var mBinding: DialogPickmeBinding
    interface Listener {
        fun onYes(data: Int)
    }

    var listener : Listener?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        mBinding = DialogPickmeBinding.inflate(LayoutInflater.from(getContext()))
        setContentView(mBinding.root)

        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        setCanceledOnTouchOutside(true)
        setCancelable(true)

        mBinding.progress.progressBar.setAnimation(R.raw.loading)

        mBinding.btnSubmit.setOnClickListener {
            mBinding.progress.progressBar.isVisible = true
            CoroutineScope(Dispatchers.Main).launch {
                delay(2000)
                mBinding.progress.progressBar.isVisible = false

                listener?.onYes(mBinding.rbRate.rating.toInt())
                dismiss()
            }
        }
    }
}