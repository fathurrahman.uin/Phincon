package kedaiapps.projeku.testandroidphincon.ui.home.adapter

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kedaiapps.projeku.testandroidphincon.BuildConfig
import kedaiapps.projeku.testandroidphincon.R
import kedaiapps.projeku.testandroidphincon.databinding.ItemHomeBinding
import kedaiapps.projeku.testandroidphincon.db.table.ListPokemonTable
import kedaiapps.projeku.testandroidphincon.ext.inflate

class AdapterHome (
    private val onClick: (ListPokemonTable) -> Unit
) : RecyclerView.Adapter<AdapterHome.ViewHolder>() {

    var items: MutableList<ListPokemonTable> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_home))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as? ViewHolder)?.bind(items.getOrNull(position)!!)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView){
        private var binding = ItemHomeBinding.bind(containerView)

        fun bind(data: ListPokemonTable){
            with(binding){

                Glide.with(itemView.rootView).load(BuildConfig.API_BASE_URL_IMG+(position+1)+".png")
                    .apply(
                        RequestOptions()
                            .transform(RoundedCorners(16))
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .dontAnimate()
                    ).into(binding.image)

                binding.judul.text = data.name

                line.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }

    fun insertData(data : List<ListPokemonTable>){
        data.forEach {
            items.add(it)
            notifyDataSetChanged()
        }
    }

    fun clearData() {
        if (items.isNotEmpty()) {
            items.clear()
            notifyDataSetChanged()
        }
    }
}