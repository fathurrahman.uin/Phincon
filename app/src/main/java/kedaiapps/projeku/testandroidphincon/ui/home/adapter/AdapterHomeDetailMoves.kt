package kedaiapps.projeku.testandroidphincon.ui.home.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kedaiapps.projeku.testandroidphincon.R
import kedaiapps.projeku.testandroidphincon.databinding.ItemHomeBinding
import kedaiapps.projeku.testandroidphincon.ext.inflate
import kedaiapps.projeku.testandroidphincon.services.entity.ResponseMoves

class AdapterHomeDetailMoves : RecyclerView.Adapter<AdapterHomeDetailMoves.ViewHolder>() {

    var items: MutableList<ResponseMoves> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_home))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as? ViewHolder)?.bind(items.getOrNull(position)!!)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView){
        private var binding = ItemHomeBinding.bind(containerView)

        fun bind(data: ResponseMoves){
            with(binding){

                binding.judul.text = data.move.name

//                line.setOnClickListener {
//                    onClick(data)
//                }
            }
        }
    }

    fun insertData(data : List<ResponseMoves>){
        data.forEach {
            items.add(it)
            notifyDataSetChanged()
        }
    }

    fun clearData() {
        if (items.isNotEmpty()) {
            items.clear()
            notifyDataSetChanged()
        }
    }
}