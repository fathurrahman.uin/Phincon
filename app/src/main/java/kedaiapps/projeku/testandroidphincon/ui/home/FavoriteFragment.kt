package kedaiapps.projeku.testandroidphincon.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.simform.refresh.SSPullToRefreshLayout
import kedaiapps.projeku.testandroidphincon.databinding.FragmentFavoriteBinding
import kedaiapps.projeku.testandroidphincon.db.table.FavoriteTable
import kedaiapps.projeku.testandroidphincon.ext.observe
import kedaiapps.projeku.testandroidphincon.modules.base.BaseFragment
import kedaiapps.projeku.testandroidphincon.ui.home.adapter.AdapterFavorite
import kedaiapps.projeku.testandroidphincon.viewmodel.RepoViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FavoriteFragment : BaseFragment() {
    lateinit var mBinding: FragmentFavoriteBinding
    private val viewModel by viewModels<RepoViewModel>()

    private val adapter by lazy(LazyThreadSafetyMode.NONE) {
        AdapterFavorite(::onClick)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentFavoriteBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initView()
        handleState()
    }

    private fun initToolbar() {
        mBinding.tlbr.apply {
            ivBack.isVisible = false
            tvTitle.text = "My Pokemon"
        }
    }

    private fun initView() {
        mBinding.ssPullRefresh.setOnRefreshListener {
            CoroutineScope(Dispatchers.Main).launch {
                delay(2000)
                mBinding.ssPullRefresh.setRefreshing(false)
                handleState()
                Toast.makeText(requireActivity(), "Refresh Complete", Toast.LENGTH_SHORT).show()
            }
        }

        mBinding.ssPullRefresh.setRefreshViewParams(
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 300)
        )

        mBinding.ssPullRefresh.setLottieAnimation("loading.json")
        mBinding.ssPullRefresh.setRepeatMode(SSPullToRefreshLayout.RepeatMode.REPEAT)
        mBinding.ssPullRefresh.setRepeatCount(SSPullToRefreshLayout.RepeatCount.INFINITE)
        mBinding.ssPullRefresh.setRefreshStyle(SSPullToRefreshLayout.RefreshStyle.NORMAL)

        mBinding.rv.adapter = adapter
    }

    private fun handleState() {
        observe(viewModel.getFavorite()) {
            if (it != null) {
                adapter.clearData()
                adapter.insertData(it)
            }
        }
    }

    private fun onClick(data: FavoriteTable) {
//        findNavController().navigate(
//            FavoriteFragmentDirections.actionFavoriteFragmentToHomeDetailFragment(
//                data.name,
//                data.fav_id
//            )
//        )
    }
}