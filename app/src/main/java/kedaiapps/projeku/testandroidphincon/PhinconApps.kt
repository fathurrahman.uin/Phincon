package kedaiapps.projeku.testandroidphincon

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PhinconApps : Application()